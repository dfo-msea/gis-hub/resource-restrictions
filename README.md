# Update resource restrictions

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Update resources for a dataset on the GIS Hub to: 

a) give access to a specific user to a restricted resource

AND/OR

b) update the level of access for a resource (such as the same organization, or only specific users)


## Summary
User must clone hub-geo-api repo to their local machine. This script is excluded from that repo since it is not 
intended to be executed on the GIS Hub server. 


## Status
Ongoing improvements.


## Contents
The script is meant to be executed from the command line. Script must be in the same directory as the cloned 
hub-geo-api scripts directory. Current directory in python must also be where the scripts are.


## Methods
This script relies on a number of functions written for the GIS Hub. Many of them user the CKAN API python functions.
Script gets the metadata for a dataset on the GIS Hub, gets a list of resources for that dataset. Resource metadata is
patched to include the new allowed user to the resource, and/or change the level of access for the 
resource. For example, you may modify a resource to only be accessible to allowed users and add users. Or you may 
set the level to the same organization. 


## Requirements
1. Must have access to private repository: https://github.com/genkimaps/hub-geo-api
2. Must copy this script to the cloned repository from above on local machine. 
3. Must create a Python environment and install all of the modules prompted after trying to import all the modules
in the import statements. 

Help (with Python env activated and in the directory where script is):

`python update_restricted.py -h`
>  -h, --help            show this help message and exit
>
>  -d DATASET, --dataset DATASET
>                        GIS Hub dataset.
>
>  -u USERNAME, --username USERNAME
>                        GIS Hub username to be added to restricted resource.
>
>  -l LEVEL, --level LEVEL
>                        Level of access for resource. Can be one of 4 string values: registered, only_allowed_users,
>                        any_organization, same_organization

**Example a)**
*Update resources in dataset substrate20m to be restricted to only allowed users and add colefields as an 
allowed user:*
1. Open an Anaconca prompt.

2. Activate python environment with install packages.
`conda activate CKAN_ENV`

3. Switch to scripts directory.
`F:\`
`cd Projects\gis-hub\Scripts\hub-geo-api`

4. Run script with parameters.
`python update_restricted.py -d substrate20m -u colefields -l only_allowed_users`

**Example b)**
*Update resources in dataset substrate20m to be restricted to only users who are members of the MSEA organization:*
1. Open an Anaconca prompt.

2. Activate python environment with install packages.
`conda activate CKAN_ENV`

3. Switch to scripts directory.
`F:\`
`cd Projects\gis-hub\Scripts\hub-geo-api`

4. Run script with parameters.
`python update_restricted.py -d substrate20m -l same_organization`

## Caveats
* Does not remove users. 
* Only works to add 1 user at a time to resource. 
* Level can only have a single value. 
* Has some error checking to make sure that the user provided has an account on the GIS Hub. Also checks that the level provided is 1 of 4 allowed values. 


## Uncertainty
* Assumes you want to update all resources that are belong to a dataset. Does not work for individual resources. 
* If only a subset of the resources need to be private, it is recommended to do that manually on the website.
* Unauthorized users will not be able to access the resource metadata or map preview by browsing to the URL.


## Acknowledgements
Michael Peterman

https://github.com/ckan/ckanapi

https://github.com/EnviDat/ckanext-restricted


## References
NA