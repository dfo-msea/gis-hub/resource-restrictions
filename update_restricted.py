# import modules.
import ckanapi
import os
import sys
from enum import Enum
import requests
from requests.exceptions import Timeout, ConnectionError, ConnectTimeout, RequestException
from json import JSONDecodeError
import json
import traceback
import settings
import re
import time
import psycopg2
from psycopg2 import extras
import s3api
import argparse


def main():
    parser = argparse.ArgumentParser('Add users to restricted resource on GIS Hub. '
                                     'Modify level of access for resource.')
    parser.add_argument('-d', '--dataset', type=str, default=None,
                        help='GIS Hub dataset name or ID.')
    parser.add_argument('-u', '--username', type=str, default=None,
                        help='GIS Hub username to be added to restricted resource.')
    parser.add_argument('-l', '--level', type=str, default=None,
                        help='Level of access for resource. Can be one of 4 string values: '
                             'registered (all registered users), only_allowed_users (only users with explicit access), '
                             'any_organization (users from any organization), same_organization (only users that belong'
                             'to the same organization that the data belongs to')
    args = parser.parse_args()

    logger = settings.setup_logger()

    project_root = os.path.realpath(os.path.dirname(__file__))
    data_folder = os.path.join(project_root, 'data')

    default_error = {'error': 'Server error'}

    # Headers for API requests, loaded at runtime.
    try:
        ghub_api_key = os.environ['GISHUB_API']
        # Set headers for API requests in settings
        ghub_headers = {'Authorization': ghub_api_key}
        logger.info('API key loaded ok. ')
    except KeyError:
        logger.error('API key GISHUB_API not found! Check environment.')
        sys.exit(1)

    # Get list of GIS Hub usernames.
    ghub_users = ckanapi.get_users()

    # Get a dataset.
    ghub_dataset = ckanapi.get_dataset(args.dataset)

    # Get a list of resources from dataset.
    ghub_resources = ckanapi.get_resources_list(ghub_dataset)

    # Add user and/or level to restricted dictionary of resource metadata.
    ckanapi.add_user_level_restricted(resources_meta_list=ghub_resources, allowed_username=args.username,
                                      level=args.level, ghub_username_list=ghub_users)


if __name__ == "__main__":
    main()
